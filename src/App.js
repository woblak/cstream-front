import './App.css';
import SseCryptoDataProvider from './components/SseCryptoDataProvider';

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
        Crypto Stream
      </header>
      <body className="App-body">
      <SseCryptoDataProvider/>
      </body>
    </div>
  );
}