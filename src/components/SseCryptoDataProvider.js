

import { useEffect, useState } from 'react';
import EventSource from 'eventsource';
import SummaryTrade from './SummaryTrade';
import MeanTradePrice from './MeanTradePrice';
import Grid from '@material-ui/core/Grid';

export default function SseCryptoDataProvider(props) {

    const CRYPTO_AGG_SSE_URL = 'http://localhost:18200/events/agg';

    const [wantedCode, setWantedCode] = useState('btcusd');
    const [summaryWindow, setSummaryWindow] = useState([]);
    const [meanWindow, setMeanWindow] = useState([]);
    const [eventSource, setEventSource] = useState();

    useEffect(() => {
        const sseUrl = getUrl(wantedCode);
        disconnnect()
        connect(sseUrl);
    }, [wantedCode])

    const connect = (url) => {
        disconnnect()
        console.log('connecting to ', url);
        const es = new EventSource(url, { withCredentials: false });
        setEventSource(e => es)

        es.on('error', handleError);

        es.addEventListener('heartBeat', handleHeartBeat);
        es.addEventListener('summary-data', handleSummaryEvent);
        es.addEventListener('meanPrice-data', handleMeanEvent);
    };

    const disconnnect = () => {
        if (eventSource) {
            eventSource.close()
            console.log("disconnected")
        }
    }

    const handleError = (e) => {
        console.log('SSE ERROR! ', e);
        disconnnect()
    }

    const handleHeartBeat = (event) => {
        console.log('SSE heartBeat: ', event);
    }

    const handleSummaryEvent = (event) => {
        const data = JSON.parse(event.data);
        if (wantedCode === Object.values(data)[0].code) {
            setSummaryWindow(x => Object.values(data));
        }
    }

    const handleMeanEvent = (event) => {
        const data = JSON.parse(event.data);
        if (wantedCode === Object.values(data)[0].code) {
            setMeanWindow(x => Object.values(data));
        }
    }

    const getUrl = (code) => {
        return CRYPTO_AGG_SSE_URL + "/" + code;
    }

    const wantedCodeChangeHandler = (event) => {
        setWantedCode(event.target.value);
        setSummaryWindow([])
        setMeanWindow([])
    };

    return (
        <div>
            <Grid container justify="flex-start">
            <Grid item xs={1}>
                </Grid>
                <Grid item xs={1}>
                    <select
                        value={wantedCode}
                        name="wantedCode"
                        className='react-select-container'
                        onChange={wantedCodeChangeHandler}
                    >
                        <option value="btcusd">BTCUSD Bitcoin/Dolar</option>
                        <option value="dotusd">DOTUSD Polkadot/Dolar</option>
                        <option value="audiousd">AUDIOUSD Audio/Dolar</option>
                    </select>
                </Grid>
            </Grid>

            <Grid container justify="flex-start">
                <Grid item xs={1}>
                </Grid>
                <Grid item xs={9}>
                    Stats based on future tumbling windows:
                    <SummaryTrade windowedValues={summaryWindow} />
                </Grid>
            </Grid>

            <Grid container justify="flex-start">
                <Grid item xs={1}>
                </Grid>
                <Grid item xs={9}>
                    Stats based on past sliding windows:
                    <MeanTradePrice windowedValues={meanWindow} />
                </Grid>
            </Grid>
        </div>
    );
}