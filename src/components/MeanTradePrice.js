import { useEffect, useState } from 'react';
import { styled } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell, { tableCellClasses } from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import Chart from "react-google-charts";
import { setMaxListeners } from 'process';

import GaugeCharts from './charts/GaugeCharts';

export default function MeanTradePrice(props) {

    const { windowedValues } = props;
    const [lines, setLines] = useState([]);
    const [linesValues, setLinesValues] = useState([[]]);

    useEffect(() => {
    }, [windowedValues])

    function parseLines(windowedValues) {
        const lines = Array.from(windowedValues.keys());
        lines.unshift("Time");
        return lines;
    }

    function parseLinesValues(windowedValues) {
        const values = Array.from(windowedValues.values());
        return values;
    }

/*
key: "PT1MPT15S"
value:
code: "dotusd"
data: {count: 4, amountSum: 2.54591, priceSum: 1043.12, priceWagedByAmountSum: 663.838301798, mean: 260.78, …}
metaData: {startMs: 1636058235000, endMs: 1636058295000}
windowDurationName: "PT1MPT15S"
*/


    return (
        <div>
            <GaugeCharts windowedValues={windowedValues} />
        </div>
    );
}