
import { useEffect, useState } from 'react';
import GaugeChart from './GaugeChart';
import Grid from '@material-ui/core/Grid';

export default function GaugeCharts(props) {

    const { windowedValues } = props;

    useEffect(() => {

    }, [windowedValues])

    function drawGaugeChart(windowedValues) {
        return windowedValues.sort((a, b) => getOrder(a) - getOrder(b))
            .map(v => {
                const chartData = parseChartData(v)
                const chartOptions = parseChartOptions(v)
                return (
                    <Grid item xs={3}>
                        <GaugeChart data={chartData} options={chartOptions} />
                    </Grid>
                );
            })
    }

    function parseChartData(v) {
        return [
            ["Label", "Value"],
            [v.windowDurationName, v.data.meanWagedByAmount]
        ]
    }

    function parseChartOptions(v) {
        const maxPrice = parseFloat(v.data.maxPrice.value)
        const minPrice = parseFloat(v.data.minPrice.value)
        const priceRange = maxPrice - minPrice

        const redFrom = maxPrice - priceRange * 0.1
        const yellowFrom = redFrom - priceRange * 0.2
        const greenTo = minPrice + priceRange * 0.2

        return {
            min: v.data.minPrice.value, max: v.data.maxPrice.value,
            redFrom: redFrom, redTo: maxPrice,
            yellowFrom: yellowFrom, yellowTo: redFrom,
            greenFrom: 0, greenTo: greenTo,
            minorTicks: 12,
            majorTicks: [minPrice, '', '', '', '', '', maxPrice],
            animation: {
                duration: 1000,
                easing: 'inAndOut'
            }
        }
    }

    function getOrder(windowedValue) {
        switch (windowedValue.windowDurationName) {
            case "PT1MPT15S":
                return 1
            case "PT10MPT1M":
                return 2
            case "PT1HPT10M":
                return 3
            case "PT24HPT1H":
                return 4
        }
    }


    return (
        <div>
            <Grid container justify="flex-start">
                {drawGaugeChart(windowedValues)}
            </Grid>
        </div >
    );
}