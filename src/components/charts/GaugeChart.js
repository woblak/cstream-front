
import { useEffect, useState } from 'react';
import Chart from "react-google-charts";

export default function GaugeChart(props) {

    const { data } = props;
    const {options} = props;

    useEffect(() => {

    }, [data, options])

    return (
        <div>
            <Chart
                width={800}
                height={240}
                chartType="Gauge"
                loader={<div>Loading Chart</div>}
                data={data}
                options={options}
                rootProps={{ 'data-testid': '1' }}
            />
        </div>
    );
}