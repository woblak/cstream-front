import { useEffect, useState } from 'react';
import { styled } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell, { tableCellClasses } from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { updateInferTypeNode } from 'typescript';


export default function SummaryTrade(props) {

    const { windowedValues } = props;

    useEffect(() => {

    }, [windowedValues])


    const useStyles = makeStyles(theme => ({
        root: {
            width: '100%',
            marginTop: theme.spacing(3),
            overflowX: 'auto',
        },
        table: {
            minWidth: 450,
        },
    }));

    function getOrder(windowedValue) {
        switch (windowedValue.windowDurationName) {
            case "PT1M":
                return 1
            case "PT10M":
                return 2
            case "PT1H":
                return 3
            case "PT24H":
                return 4
        }
    }

    function parseDate(timestamp) {
        const timestampDate = new Date(timestamp)

        const [year, month, day, hours, minutes, seconds] = [
            timestampDate.getFullYear(),
            unify(timestampDate.getMonth()),
            unify(timestampDate.getDay()),
            unify(timestampDate.getHours()),
            unify(timestampDate.getMinutes()),
            unify(timestampDate.getSeconds())
        ]

        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
    }

    function unify(number) {
        return number < 10 ? "0" + number : "" + number
    }

    const classes = useStyles();

    return (
        <Paper className={classes.root}>
            <Table className={classes.table} aria-label="caption table">
                <caption>Summary transaction stats</caption>
                <TableHead>
                    <TableRow>
                        <TableCell align="right">Window name</TableCell>
                        <TableCell align="right">Update date</TableCell>
                        <TableCell align="right">window from</TableCell>
                        <TableCell align="right">window to</TableCell>
                        <TableCell align="right">transactions count</TableCell>
                        <TableCell align="right">min price</TableCell>
                        <TableCell align="right">max price</TableCell>
                        <TableCell align="right">max amount quote</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {Array.from(windowedValues)
                        .sort((a, b) => getOrder(a) - getOrder(b))
                        .map(row => (
                            <TableRow key={row.windowDurationName}>
                                <TableCell align="right">{row.windowDurationName}</TableCell>
                                <TableCell align="right">{parseDate(row.data.count.timestamp)}</TableCell>
                                <TableCell align="right">{parseDate(row.metaData.startMs)}</TableCell>
                                <TableCell align="right">{parseDate(row.metaData.endMs)}</TableCell>
                                <TableCell align="right">{row.data.count.value}</TableCell>
                                <TableCell align="right">{row.data.minPrice.value.toFixed(2)}</TableCell>
                                <TableCell align="right">{row.data.maxPrice.value.toFixed(2)}</TableCell>
                                <TableCell align="right">{row.data.maxAmountQuote.value.toFixed(2)}</TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </Paper>
    );
}